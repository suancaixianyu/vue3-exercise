# vue3 练习

#### 介绍

vue3 学习之路

#### 安装教程

1. 克隆项目，进入项目目录安装依赖

```
git clone https://gitee.com/suancaixianyu/vue3-exercise.git
cd vue3-exercise
pnpm i
```

2. 启动项目

```
npm run serve
```
