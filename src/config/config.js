import { reactive } from 'vue'
import Method from '../globalmethods'

const homestyle = reactive({
  /** 内容样式 */
  maincontainer: {
    padding: '0px 20px',
    height: 'calc(100vh - 90px)',
    overflowY: 'hidden',
  },
  /** 页面整体样式 */
  container: {
    padding: '0px',
  },

  cfg: {
    menu: true,
    isshow: true,
  },
})

const userInfo = reactive({
  isLogin: false,
  isLoginDialogVisible: false,
  data: {},
})

class Cfg {
  constructor() {
    this.config = {
      /** 后端服务器地址 */
      server: 'https://schub.top',
      /** 主页样式 */
      homestyle,
      /** 用户信息 */
      userInfo,
    }
  }

  getHostUrl(e) {
    if (e.indexOf('http://') != -1 || e.indexOf('https://') != -1) return e
    if (e.indexOf('./') == -1) {
      return this.config.server + e
    } else {
      return this.config.server + e.substr(1, e.length)
    }
  }

  getInformation() {
    //刷新页面重新获取用户信息
    Method.api_get('/user/info').then((response2) => {
      if (response2.data.code == 200) {
        userInfo.isLogin = true
        userInfo.isLoginDialogVisible = false
        response2.data.data.headurl = this.getHostUrl(response2.data.data.headurl)
        userInfo.data = response2.data.data
      }
    })
  }
}

export default new Cfg()
