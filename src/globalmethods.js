import Cfg from './config/config'
import axios from 'axios'

//开启cookie携带
axios.defaults.withCredentials = true

class global {
  constructor() {
    this._config = {
      // 配置信息
      version: '1.0.0',
    }
    this.axios = axios
  }
  /**
   * get方法请求api
   * @param {string} path 请求路径
   */
  api_get(path) {
    if (path) return axios.get(`${Cfg.config.server}${path}`)
  }

  /**
   * post方法请求api
   * @param {string} path 请求路径
   * @param {object} data 请求体
   */
  api_post(path, data) {
    return axios.post(`${Cfg.config.server}${path}`, data)
  }

  /**
   * 格式化时间戳 -> xx时间前
   * @param {number} time 时间戳
   */
  formatBbsTime(time) {
    if (typeof time === 'string') time = time * 1
    let time2 = new Date()
    time = parseFloat(time * 1000)
    const diffMilliseconds = Math.abs(time2 - time)
    const diffSeconds = Math.floor(diffMilliseconds / 1000)
    const diffMinutes = Math.floor(diffSeconds / 60)
    const diffHours = Math.floor(diffMinutes / 60)
    const diffDays = Math.floor(diffHours / 24)
    if (diffDays >= 1) {
      time = `${diffDays}天前`
    } else if (diffHours % 24 >= 1) {
      time = `${diffHours % 24}小时前`
    } else if (diffMinutes % 60 >= 1) {
      time = `${diffMinutes % 60}分钟前`
    } else {
      time = `${diffSeconds % 60}秒前`
    }
    return time
  }

  /**
   * 资源类型
   * @param {number} type 类型id
   */
  getScTypeName(type) {
    switch (type) {
      case 1:
        return '世界'
      case 2:
        return '方块材质'
      case 3:
        return '人物皮肤'
      case 4:
        return '家具包'
      default:
        return '未知'
    }
  }

  /**
   * 获取文件后缀
   * @param {string} name 文件名
   */
  getFileName(name) {
    let i = name.lastIndexOf('.')
    return name.substring(0, i)
  }

  fixTimePre(str) {
    if (str < 10) return '0' + str
    else return str
  }

  strtotime(date = null) {
    if (date == null) return parseInt(Date.now() / 1000)
    else {
      date = date.replaceAll('-', '/') //解决IOS不兼容问题
      let d = Date.parse(date)
      return parseInt(d / 1000)
    }
  }

  myDate(timestamp, format) {
    if (format == null) {
      format = timestamp
      timestamp = this.strtotime()
    }
    let d = new Date(timestamp * 1000)
    return format
      .replace('Y', d.getFullYear())
      .replace('m', this.fixTimePre(d.getMonth() + 1))
      .replace('d', this.fixTimePre(d.getDate()))
      .replace('H', this.fixTimePre(d.getHours()))
      .replace('i', this.fixTimePre(d.getMinutes()))
      .replace('s', this.fixTimePre(d.getSeconds()))
  }

  /**
   * 格式化时间戳 -> Y-m-d H:i:s
   * @param {number} time 时间戳
   */
  formatNormalTime(time) {
    if (typeof time === 'string') time = time * 1
    return this.myDate(time, 'Y-m-d H:i:s')
  }

  /**
   * 读取本地信息
   */
  localGet(key,defaultValue){
    let v;
    if((v=localStorage.getItem(key))==null)return defaultValue;
    else return JSON.parse(v);
  }

  /**
   * 储存信息到本地
   */
  localSet(key,obj){
    localStorage.setItem(key,JSON.stringify(obj));
  }
}

export default new global()
