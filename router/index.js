import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  /** 主页 */
  {
    path: '/',
    name: 'PlateBox',
    component: ()=>import("@/components/main/PlateBox"),//路由懒加载
  },
  /** 帖子列表 */
  {
    path: '/postlist/:chatid',
    name: 'PostPage',
    component: ()=>import("@/components/main/PostPage"),
    children: [
      {
        name: 'xiangqing',
        path: ':id',
        component: ()=>import("@/components/main/cards/DetailPlate"),
      },
    ],
  },
  /** 用户主页 */
  {
    path: '/user',
    name: 'UserIndex',
    component: ()=>import("@/components/main/UserIndex"),
  },

  {
    path: '/setup',
    name: 'UserSetup',
    component: ()=>import("@/components/main/UserSetup"),
  },
  /** 登录和注册 */
  {
    path: '/login/:register?',
    name: 'UserLogin',
    component: ()=>import("@/components/main/UserLogin"),
  },
  /** 发帖 */
  {
    path: '/publish/:chatid',
    name: 'publish',
    component: ()=>import("@/components/main/PublishPost"),
  },
  /**发模组*/
  {
    path:'/ModPublish',
    name:'ModPublish',
    component: ()=>import("@/components/mod/publish")
  },
  /**模组列表 or 资源大厅*/
  {
    path:'/ModList',
    name:'ModList',
    component: ()=>import("@/components/mod/list")
  },
  {
    path:"/ModDetail/:id",
    name:'ModDetail',
    component:()=>import("@/components/mod/detail")
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router
